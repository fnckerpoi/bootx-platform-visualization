import {ModuleTypeEnum} from '@/extension/enums/ModuleTypeEnum'

// 接口白名单（免登录）
export const fetchAllowList = [
  // 登录
  `${ModuleTypeEnum.SYSTEM}/login`,
  // 获取 OSS 接口
  `${ModuleTypeEnum.GO_VIEW}/getOssInfo`,
  // 预览获取数据
  `${ModuleTypeEnum.GO_VIEW}/getPublishData`,
]

// 接口黑名单
export const fetchBlockList = []
