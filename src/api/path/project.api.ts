import {http} from '@/api/http'
import {httpErrorHandle} from '@/utils'
import {ContentTypeEnum, RequestHttpEnum} from '@/enums/httpEnum'
import {ProjectDetail, ProjectItem} from './project'
import {ModuleTypeEnum} from '@/extension/enums/ModuleTypeEnum'

/**
 * 项目列表
 */
export const projectListApi = async (data: object) => {
  try {
      return await http(RequestHttpEnum.GET)<ProjectItem[]>(`${ModuleTypeEnum.GO_VIEW}/page`, data)
  } catch {
    httpErrorHandle()
  }
}

/**
 * 新增项目 不再使用
 */
export const createProjectApi = async (data: object) => {
  try {
      return await http(RequestHttpEnum.POST)<{
        /**
         * 项目id
         */
        id: number
    }>(`${ModuleTypeEnum.GO_VIEW}/create`, data)
  } catch {
    httpErrorHandle()
  }
}

/**
 * 获取项目 预览编辑时信息
 */
export const fetchProjectEditApi = async (data: object) => {
  try {
      return await http(RequestHttpEnum.GET)<ProjectDetail>(`${ModuleTypeEnum.GO_VIEW}/getEditData`, data)
  } catch {
    httpErrorHandle()
  }
}

/**
 * 获取项目 预览发布后信息
 */
export const fetchProjectPublishApi = async (data: object) => {
  try {
      return await http(RequestHttpEnum.GET)<ProjectDetail>(`${ModuleTypeEnum.GO_VIEW}/getPublishData`, data)
  } catch {
    httpErrorHandle()
  }
}

/**
 * 保存项目
 */
export const saveProjectApi = async (data: object) => {
  try {
      return await http(RequestHttpEnum.POST)(
        `${ModuleTypeEnum.GO_VIEW}/update`,
        data,
    )
  } catch {
    httpErrorHandle()
  }
}

/**
 * 删除项目 不再使用
 */
export const deleteProjectApi = async (data: object) => {
  try {
      return await http(RequestHttpEnum.DELETE)(`${ModuleTypeEnum.GO_VIEW}/delete`, data)
  } catch {
    httpErrorHandle()
  }
}

/**
 * 修改发布状态 [-1未发布,1发布] 不再使用
 */
export const changeProjectReleaseApi = async (data: object) => {
  try {
      return await http(RequestHttpEnum.PUT)(`${ModuleTypeEnum.GO_VIEW}/publish`, data)
  } catch {
    httpErrorHandle()
  }
}

/**
 * 上传文件
 */
export const uploadFile = async (data: object) => {
  try {
      return await http(RequestHttpEnum.POST)<{
        // 文件id
        id: string
    }>(`/file/upload`, data, ContentTypeEnum.FORM_DATA)
  } catch {
    httpErrorHandle()
  }
}
