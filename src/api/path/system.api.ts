import {http} from '@/api/http'
import {httpErrorHandle} from '@/utils'
import { RequestHttpEnum } from '@/enums/httpEnum'
import {ModuleTypeEnum} from '@/extension/enums/ModuleTypeEnum'

// * 登录
export const loginApi = async (data: object) => {
  try {
      return await http(RequestHttpEnum.POST)<string>(`token/login`,undefined,undefined, data)
  } catch (err) {
    httpErrorHandle()
  }
}

// * 登出
export const logoutApi = async () => {
  try {
      return await http(RequestHttpEnum.POST)(`/token/logout`)
  } catch (err) {
    httpErrorHandle()
  }
}

// * 获取 oss 上传接口
export const ossUrlApi = async (data: object) => {
  try {
      return await http(RequestHttpEnum.GET)<{
        /**
         * bucket 地址
         */
        bucketURL?: string
    }>(`${ModuleTypeEnum.GO_VIEW}/getOssInfo`, data)
  } catch (err) {
    httpErrorHandle()
  }
}
