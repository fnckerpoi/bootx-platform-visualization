// 模块 Path 前缀分类
export enum ModuleTypeEnum {
    SYSTEM = 'sys',
    PROJECT = 'project',
    GO_VIEW = 'goview',
}
